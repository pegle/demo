import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  newTitle = '';
  list = [
    'Äpfel',
    'Tomaten',
  ];

  constructor() { }

  ngOnInit() {
  }

  onAdd() {
    if (this.newTitle) {
      this.list.push(this.newTitle);
      this.newTitle = '';
    }
  }

  onDeleteButton(index) {
    this.list.splice(index, 1);
  }

}
